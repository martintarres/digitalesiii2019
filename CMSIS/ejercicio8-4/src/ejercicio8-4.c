#include "LPC17xx.h"

void ConfPines(void); //configuracion de pines
void ConfIntExt(void); //Configuracion de Interrupcion por GPIO.
void ConfTimer0(void); //Configuracion de Interrupcion por TMR0.
void retardo(uint32_t);
uint32_t carga=200000;

int main(void) {

	ConfPines();
	ConfIntExt();
	ConfTimer0();
	LPC_TIM0->TCR = 1;

    while(1) {
    		LPC_TIM0->MR0=carga;
    	}
    return 0;
}

void ConfPines(void){
	LPC_GPIO0->FIODIR |= (1<<22); //SALIDA
	LPC_GPIO2->FIODIR &=~(1<<12); //ENTRADA DEL PULSADOR P2.0
	return;
}

void ConfIntExt(void){
	LPC_PINCON->PINSEL4|=(1<<24); // P.2.12 COMO EINT2
	LPC_SC->EXTINT|=(1<<2);//LIMPIO BANDERA
	LPC_SC->EXTMODE|=(1<<2);//MODO FLANCO.
	LPC_SC->EXTPOLAR&=~(1<<2); //FLANCO DE BAJADA.
	NVIC_SetPriority(EINT2_IRQn,2); //Nivel de prioridad de 2.
	NVIC_EnableIRQ(EINT2_IRQn);//HABILITO INTERRUPCION EXTERNA;
	return;
}

void ConfTimer0(void){
	LPC_SC->PCONP|=(1<<1);//Timer/Counter 0 power/clock control bit.
	LPC_SC->PCLKSEL0|=(1<<2);//Peripheral clock selection for TIMER0. 100MHz
	LPC_TIM0->MR0=200000;//CUENTA HASTA ACA
	LPC_TIM0->MCR=3;//Interrupci贸n en MR0: se genera una interrupci贸n cuando MR0 coincide con el valor en el TC.
					//el TC se restablecer谩 si MR0 coincide.
	LPC_TIM0->TC=0;//LA CUENTA EMPIEZA DESDE 0.
	NVIC_SetPriority(TIMER0_IRQn,5);//Nivel de prioridad de 5
	NVIC_EnableIRQ(TIMER0_IRQn);// HABILITO LA INTERRUPCION DE TIMER0
	return;
}

void retardo(uint32_t tiempo){
	uint32_t conta;
	for(conta=0;conta < tiempo;conta++){}
	return;
}

void EINT2_IRQHandler (void){
	retardo(100000);
	if(carga*2<=10000000){
		carga=carga*2; //por cada interrupcion externa aumento el valor del MR0 al doble.
	}else{
		carga=200000;
	}

	LPC_SC->EXTINT|=(1<<2);//LIMPIO BANDERA
}

void TIMER0_IRQHandler (void){
	static uint8_t i = 0;
		if (i==0){
			LPC_GPIO0->FIOSET = (1<<22);
			i = 1;
		}
		else if (i==1){
			LPC_GPIO0->FIOCLR = (1<<22);
			i = 0;
		}
		LPC_TIM0->IR|=1; //Limpia bandera de interrupci贸n
		return;
}
