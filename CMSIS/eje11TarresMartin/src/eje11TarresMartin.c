/*
===============================================================================
 Name        : eje11TarresMartin.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
===============================================================================
*/

#include "LPC17xx.h"

void ConfPines(void); //Configuracion de pines
void ConfIntExt(void); //Configuracion de las interrupciones externas
void ConfTimer0(void); //Configuracion del timer0
void retardo(uint32_t); //funcion retardo para el rebote de los botones
void display(uint32_t); //funcion para manejar los numeros en los display
uint32_t contador=5000000; // variable para manejar el tiempo de interrupciones del SysTick // empieza en 50mseg
uint8_t unidad=0;
uint8_t decena=0;
uint8_t numeroDisplay=1;

int main(void) {

	ConfPines();
	ConfIntExt();

	if(SysTick_Config(contador)){ //primeros verificamos si el valor es valido
	while(1);// aviso de error.
	}
	ConfTimer0();
    while(1) { }
    return 0 ;
}
void ConfPines(void){
	LPC_GPIO0->FIODIR |= (1<<9); //llave display 1
	LPC_GPIO0->FIODIR |= (1<<8); //llave display 2
	LPC_GPIO2->FIODIR |= (0xFF<<2); // 7 segmentos del P2.2 al P2.8
	LPC_GPIO2->FIODIR &=~(1<<10); //EINT0
	LPC_GPIO2->FIODIR &=~(1<<11); //EINT1
	LPC_PINCON->PINMODE4 |=(0xF<<20); // activo res pull dowm para los pulsadores

	return;
}

void ConfIntExt(void){
	LPC_PINCON->PINSEL4|=(1<<20); //P2.10 COMO EINT0
	LPC_PINCON->PINSEL4|=(1<<22); //P2.11 COMO EINT1

	LPC_SC->EXTINT|=(1<<0); // LIMPIO BANDERA PARA EINT0
	LPC_SC->EXTINT|=(1<<1); // LIMPIO BANDERA PARA EINT1

	LPC_SC->EXTMODE|=(1<<0); // MODO FLANCO PARA EINT0
	LPC_SC->EXTMODE|=(1<<1); // MODO FLANCO PARA EINT1

	LPC_SC->EXTPOLAR|=(1<<0); // FLANCO DE SUBIDA PARA EINT0
	LPC_SC->EXTPOLAR|=(1<<1); // FLANCO DE SUBIDA PARA EINT1

	NVIC_EnableIRQ(EINT0_IRQn); // activo interrupcion por EINT0.
	NVIC_EnableIRQ(EINT1_IRQn); // activo interrupcion por EINT1.
	return;
}

void ConfTimer0(void){
	LPC_SC->PCONP|=(1<<1);//Timer/Counter 0 power/clock control bit.
	LPC_SC->PCLKSEL0 |=(1<<2);//Peripheral clock selection for TIMER0. 100MHz
	LPC_TIM0->MR0=100000000;//CUENTA HASTA ACA. VALOR DE 10M PARA QUE LA  INTERRUPCION SE PRODUSCA CADA 1 SEG
	LPC_TIM0->MCR=3;//InterrupciON en MR0: se genera una interrupciOn cuando MR0 coincide con el valor en el TC.
					//el TC se restablecera si MR0 coincide.
	LPC_TIM0->TCR = 1;
	NVIC_SetPriority(TIMER0_IRQn,1);//Nivel de prioridad de 1
	NVIC_EnableIRQ(TIMER0_IRQn);// HABILITO LA INTERRUPCION DE TIMER0
}

void retardo(uint32_t tiempo){
	uint32_t conta;
	for(conta=0; conta<tiempo;conta++){}
	return;
}

void EINT0_IRQHandler(void){ // Aumento el tiempo de multiplexado
	retardo(10000);
	if(contador+500000<=9500000){ //Verifico que no se pase de 95mseg
		contador=contador+500000;
	}
	SysTick -> LOAD = contador; // cargo el nuevo valor
	LPC_SC->EXTINT|=(1<<0); // LIMPIO BANDERA
	return;
}

void EINT1_IRQHandler(void){ // disminuyo el tiempo de multiplexado
	retardo(10000);
	if(contador-500000>=500000){ //Verifico que no disminuya menos de 5mseg
		contador=contador-500000;
	}
	SysTick -> LOAD = contador; // cargo el nuevo valor
	LPC_SC->EXTINT|=(1<<0); // LIMPIO BANDERA
	return;
}

void SysTick_Handler(void){
	if(numeroDisplay==1){ //Multiplexado de display.
		display(unidad);
		numeroDisplay=2;
		LPC_GPIO0->FIOSET=(1<<8);
    	LPC_GPIO0->FIOCLR=(1<<9);
	}else {
		display(decena);
		numeroDisplay=1;
    	LPC_GPIO0->FIOCLR=(1<<8);
    	LPC_GPIO0->FIOSET=(1<<9);
	}
	SysTick->CTRL &= SysTick->CTRL;
return;
}

void TIMER0_IRQHandler(void){
	if(unidad<9){
		unidad++;
	}else{   //si unidad es mayor a 9 reinicio unidad y le sumo uno a decena
		decena++;
		unidad=0;
	}

	if(unidad==9 && decena==5){ //si el numero es 59 , vuelve a empezar a 0
		unidad=0;
		decena=0;
	}
	LPC_TIM0->IR|=1; //Limpia bandera de interrupciÃ³n
			return;
}

void display(uint32_t n){
	switch(n){ //Primero apago todo los segmentos y luego prendo el numero correspondiente
		case 0:									//0
			LPC_GPIO2->FIOCLR |= (0xFF<<2);
			LPC_GPIO2->FIOSET |= (0x3F<<2);
			break;
		case 1:									//2
			LPC_GPIO2->FIOCLR |= (0xFF<<2);
			LPC_GPIO2->FIOSET |= (0x06<<2);
			break;
		case 2:									//3
			LPC_GPIO2->FIOCLR |= (0xFF<<2);
			LPC_GPIO2->FIOSET |= (0x5B<<2);
			break;
		case 3:									//3
			LPC_GPIO2->FIOCLR |= (0xFF<<2);
			LPC_GPIO2->FIOSET |= (0x4F<<2);
			break;
		case 4:									//4
			LPC_GPIO2->FIOCLR |= (0xFF<<2);
			LPC_GPIO2->FIOSET |= (0x66<<2);
			break;
		case 5:									//5
			LPC_GPIO2->FIOCLR |= (0xFF<<2);
			LPC_GPIO2->FIOSET |= (0x6D<<2);;
			break;
		case 6:									//6
			LPC_GPIO2->FIOCLR |= (0xFF<<2);
			LPC_GPIO2->FIOSET |= (0x7D<<2);;
			break;
		case 7:									//7
			LPC_GPIO2->FIOCLR |= (0xFF<<2);
			LPC_GPIO2->FIOSET |= (0x07<<2);;
			break;
		case 8:									//8
			LPC_GPIO2->FIOCLR |= (0xFF<<2);
			LPC_GPIO2->FIOSET |=(0x7F<<2);;
			break;
		case 9:									//9
			LPC_GPIO2->FIOCLR |= (0xFF<<2);
			LPC_GPIO2->FIOSET |= (0x67<<2);
			break;
	}

}





