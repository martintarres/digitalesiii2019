#include "LPC17xx.h"

void ConfiguracionPines(void); // funcion para la configuracion de pines.
void SysTick_Handler(void);

uint8_t inte=0;

int main(void) {

	ConfiguracionPines();

	if(SysTick_Config(SystemCoreClock/10)){
		while(1);
	}

    while(1) {
        if(inte%2){
        	LPC_GPIO0->FIOSET |=(1<<22);
        }
        else
        {
        	LPC_GPIO0->FIOCLR |= (1<<22);
        }
    }
    return 0 ;
}

void ConfiguracionPines(void) {
	LPC_GPIO0-> FIODIR |=(1<<22);
	return;
}

void SysTick_Handler(void){
	inte++;
	SysTick->CTRL &= SysTick->CTRL;
}
