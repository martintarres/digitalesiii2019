/*
===============================================================================
 Name        : eje6TarresMartin.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
===============================================================================
*/


#include "LPC17xx.h"

void configGPIO(void); // funcion para la configuracion de pines.
void SysTick_Handler(void);

uint8_t medioSeg=0;
uint8_t Seg=0;
uint8_t inte=0;

int main(void) {

	configGPIO();

	if(SysTick_Config(SystemCoreClock/6)){
		while(1);
	}

    while(1) {

    	if(inte <3){
    		LPC_GPIO0->FIOCLR |=(1<<22);
    	}

    	if(inte <9 && inte >2){
    		LPC_GPIO0->FIOSET |=(1<<22);
    	}

    	if(inte==9){
    		inte=0;
    	}

    }
    return 0 ;
}

void configGPIO(void) {
	LPC_GPIO0-> FIODIR |=(1<<22);
	return;
}

void SysTick_Handler(void){
	inte++;
	SysTick->CTRL &= SysTick->CTRL;
}
