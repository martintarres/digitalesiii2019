/*
===============================================================================
 Name        : ejemploADC.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
===============================================================================
*/

#include "LPC17xx.h"

void configGPIO(void);
void configADC(void);

int main(void) {

	configGPIO();
	configADC();
	while(1){

	}
}

void configGPIO(void){
	LPC_GPIO0->FIODIR |= (1<<22);
}

void configADC(void){
	LPC_SC->PCONP |= (1<<12);
	LPC_ADC->ADCR |= (1<<21);
	LPC_SC->PCLKSEL0 |= (3<<24);
	LPC_ADC->ADCR &= ~(255<<8);
	LPC_ADC->ADCR |= (1<<16);
	LPC_PINCON->PINMODE1 |= (1<<15);
	LPC_PINCON->PINMODE1 |= (1<<14);
	LPC_ADC->ADINTEN = 1;
	NVIC_EnableIRQ(ADC_IRQn);
}

void ADC_IRQHandler(void){
	static uint16_t ADC0Value=0;
	ADC0Value = ((LPC_ADC->ADDR0)>>4) & 0xFFF;

	if(ADC0Value<2045){
		LPC_GPIO0->FIOSET = (1<<22);
	}else{
		LPC_GPIO0->FIOCLR = (1<<22);
	}
}
