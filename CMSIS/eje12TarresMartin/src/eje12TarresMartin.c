/*
===============================================================================
 Name        : eje12TarresMartin.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
===============================================================================
*/

#include "LPC17xx.h"

void configGPIO(void);
void configADC(void);

int main(void) {

	configGPIO();
	configADC();
	while(1){

	}
}

void configGPIO(void){
	LPC_GPIO0->FIODIR |= (1<<22);
}

void configADC(void){
	//HABILITO CLOCK PARA ADC
	LPC_SC->PCONP |= (1<<12);
	//ADC EN MODO OPERACIONAL
	LPC_ADC->ADCR |= (1<<21);
	//PCLK=CCLK/8 = 100MHz/8 -> PCLKadc=12.5MHz
	LPC_SC->PCLKSEL0 |= (3<<24);
	//DIVIDO AL ADC POR ESTE VALOR PARA TENER EL CLOCK CO EL QUE EL ADC CONVIERTE
	//QUE SERIA 0. ESTO FUNCIONA DEBIDO A QUE EL ADC DEBE A VALORES CERCANOS A 13MHzM Y YO ESTOY EN 12.5
	LPC_ADC->ADCR &= ~(255<<8);
	//EL ADC FUNCIONA EN MODO BURST CON FRECUENCIA DE MUESTREO A 200K
	LPC_ADC->ADCR |= (1<<16);
	//PULL DOWN
	LPC_PINCON->PINMODE1 |= (1<<15);
	LPC_PINCON->PINMODE1 |= (1<<14);
	//GENERA INTERRUPCION CUANDO TERMINO CONVERSION POR CANAL 0
	LPC_ADC->ADINTEN = 1;
	//HABILITO INTERRUPCION POR ADC
	NVIC_EnableIRQ(ADC_IRQn);
}

void ADC_IRQHandler(void){
	static uint16_t ADC0Value=0;
	ADC0Value = ((LPC_ADC->ADDR0)>>4) & 0xFFF;

	if(ADC0Value<2045){
		LPC_GPIO0->FIOSET = (1<<22);
	}else{
		LPC_GPIO0->FIOCLR = (1<<22);
	}
}
