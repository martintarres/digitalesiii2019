/*
===============================================================================
 Name        : teorico1.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
===============================================================================
*/

#ifdef __USE_CMSIS
#include "LPC17xx.h"
#endif

#include <cr_section_macros.h>

void configPines();
int contadorToques=0;
int contadorSystick=0;
int flagLedsCorrectos=0; 				//variable que sirve para ver si los 3 led andan

int main(void) {
	configPines();
	//apago los led por si quedaron prendidos de antes
	LPC_GPIO0->FIOCLR |= (1<<8);
	LPC_GPIO0->FIOCLR |= (1<<9);
	LPC_GPIO0->FIOCLR |= (1<<7);

	LPC_SC -> CCLKCFG =3;
    while(1) {
    	int a;
    	if((LPC_GPIO0->FIOPIN)&(1<<10)){

    	}else{
    		contadorToques++;
    		switch(contadorToques){
    			case 1:{
    				inicioLed();
    				break;
    			}
    			case 2:{
    				SysTick->CTRL &= ~SysTick_CTRL_ENABLE_Msk;
    				LPC_GPIO0->FIOSET |= (1<<9);
    				break;
    			}
    			case 3:{
    				LPC_GPIO0->FIOCLR |= (1<<9);
    				LPC_GPIO0->FIOSET |= (1<<8);
    				break;
    			}
    			case 4:{
    				LPC_GPIO0->FIOCLR |= (1<<8);
    				LPC_GPIO0->FIOSET |= (1<<7);
    				break;
    			}
    			case 5:{
    				LPC_GPIO0->FIOCLR |= (1<<7);
    				inicioLed();
    				break;
    			}
    		}
    	}
    }
    return 0 ;
}

void configPines(){
	//Pongo pines 7,8 y 9 del puerto 0 como salida
	LPC_GPIO0->FIODIR |= (1<<9);
	LPC_GPIO0->FIODIR |= (1<<8);
	LPC_GPIO0->FIODIR |= (1<<7);
	//Pongo pin 10 del puerto 0 como entrada
	LPC_GPIO0->FIODIR &= ~(1<<10);
}

void retardoLed(){
	int a;
	for(int i=0;i<1000000;i++){
		a++;
	}
}

void inicioLed(){
	LPC_GPIO0->FIOSET |= (1<<9);
	retardoLed();
	LPC_GPIO0->FIOCLR |= (1<<9);
	retardoLed();
	LPC_GPIO0->FIOSET |= (1<<8);
	retardoLed();
	LPC_GPIO0->FIOCLR |= (1<<8);
	retardoLed();
	LPC_GPIO0->FIOSET |= (1<<7);
	retardoLed();
	LPC_GPIO0->FIOCLR |= (1<<7);
	retardoLed();
	if(contadorSystick==0){
		SysTick_Config(16000000);
	}

}



void SysTick_Handler(void)  {
	if(contadorSystick==10){
		inicioLed();
		SysTick->CTRL &= ~SysTick_CTRL_ENABLE_Msk;
		contadorSystick=0;
	}else{
		contadorSystick++;
	}

}
