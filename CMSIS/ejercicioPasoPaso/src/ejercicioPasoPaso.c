/*
===============================================================================
 Name        : ejercicioPasoPaso.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
===============================================================================
*/

#include "LPC17xx.h"

void confGPIO(void); // Prototipo de la funcion de conf. de puertos
void confTimer(void); //Prototipo de la función de conf. de interrupciones externas

int main(void) {
	confGPIO();
	confTimer();
	while(1){}
	return 0;
}

void confGPIO(void){
	LPC_GPIO0->FIODIR |= (0xF<<6);
	return;
}

void confTimer(void){
	LPC_SC->PCONP |= (1 << 1); //Por defecto timer 0 y 1 estan siempre prendidos
	LPC_SC->PCLKSEL0 |= (1<<2); // configuración del clock de periférico
	LPC_TIM0->MR0 = 30000000;
	LPC_TIM0->MR1 = 60000000;
	LPC_TIM0->MR2 = 90000000;
	LPC_TIM0->MR3 = 120000000;
	LPC_TIM0->MCR = (0x649<<0);
	LPC_TIM0->TCR = (1<<0);
	NVIC_EnableIRQ(TIMER0_IRQn);
	return;
}


void TIMER0_IRQHandler(void){ //ISR del timer0
	if(LPC_TIM0 -> IR &(1<<0)){
		LPC_TIM0-> IR |= (1<<0);
		LPC_GPIO0->FIOSET = (1<<9);
		LPC_GPIO0->FIOCLR = (1<<6);
	}

	if(LPC_TIM0 -> IR &(1<<1)){
		LPC_TIM0-> IR |= (1<<1);
		LPC_GPIO0->FIOSET = (1<<8);
		LPC_GPIO0->FIOCLR = (1<<9);
	}

	if(LPC_TIM0 -> IR &(1<<2)){
		LPC_TIM0-> IR |= (1<<2);
		LPC_GPIO0->FIOSET = (1<<7);
		LPC_GPIO0->FIOCLR = (1<<8);
	}

	if(LPC_TIM0 -> IR &(1<<3)){
		LPC_TIM0-> IR |= (1<<3);
		LPC_GPIO0->FIOSET = (1<<6);
		LPC_GPIO0->FIOCLR = (1<<7);
	}
}
