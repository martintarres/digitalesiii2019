/*
===============================================================================
 Name        : EjemploInterr.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
===============================================================================
*/


#include "LPC17xx.h"

void confGPIO(void);
void confIntExt(void);
void retardo(uint32_t tiempo);
void EINT0_IRQHandler(void);

uint8_t inte=0;


int main(void) {
	uint32_t tiempo;
	confGPIO();
	confIntExt();

    while(1) {
        if(inte%2)
        {
        	tiempo=1000000;
        }
        else{
        	tiempo=4000000;
        }
        LPC_GPIO0->FIOSET=(1<<22);
        retardo(tiempo);
        LPC_GPIO0->FIOCLR=(1<<22);
        retardo(tiempo);
    }
    return 0 ;
}

void retardo(uint32_t tiempo){
	for(uint32_t conta=0; conta < tiempo; conta++){

	}
}

void confGPIO(void){
	LPC_GPIO0->FIODIR |=(1<<22);
	//LPC_GPIO0->FIODIR &=~(1<<15);
	return;
}

void confIntExt(void){
	LPC_PINCON->PINSEL4 |= (1<<20);
	LPC_SC-> EXTINT |= 1;
	LPC_SC-> EXTMODE |= 1;
	LPC_SC-> EXTPOLAR |= 1;
	NVIC_EnableIRQ(EINT0_IRQn);
	return;
}

void EINT0_IRQHandler(void){

	inte++;
	LPC_SC-> EXTINT |=1;
	return;
}
