/*
===============================================================================
 Name        : eje9_1TarresMartin.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
===============================================================================
*/

#include "LPC17xx.h"

void confPines(void); // Prototipo de la funcion de conf. de puertos
void confTimer(void); //Prototipo de la función de conf. de interrupciones externas
void confInterr(void);
void retardo(uint32_t);
uint8_t boolModo=0;
uint8_t contador=0;

int main(void) {
	confPines();
	confInterr();
	confTimer();
	while(1){}
	return 0;
}

void confPines(void){
	LPC_GPIO0->FIODIR |= (0xF<<6);
	LPC_GPIO0->FIOCLR |= (0xF<<6);
	LPC_GPIO2 -> FIODIR &= ~(1<<13);	//PONGO COMO ENTRADA EL PIN TRECE DEL PUERTO DOS
	return;
}

void confInterr(void){
	LPC_PINCON -> PINSEL4 &= ~(1<<27);	//SELECCIONO EINT2
	LPC_PINCON -> PINSEL4 |= (1<<26);	//SELECCIONO EINT2
	LPC_SC -> EXTMODE |= (1<<3);			//EINT1 POR FLANCOS
	LPC_SC -> EXTPOLAR &= ~(1<<3);			//EINT1 POR FLANCOS DESCENDENTE
	LPC_SC->EXTINT|=(1<<2);	//LIMPIO BANDERA
	NVIC_EnableIRQ(EINT3_IRQn);
}

void confTimer(void){
	LPC_SC->PCONP |= (1 << 1); //Por defecto timer 0 y 1 estan siempre prendidos
	LPC_SC->PCLKSEL0 |= (1<<2); // configuración del clock de periférico
	LPC_TIM0->MR0 = 50000000;
	LPC_TIM0->MR1 = 100000000;
	LPC_TIM0->MR2 = 150000000;
	LPC_TIM0->MR3 = 200000000;
	LPC_TIM0->MCR = (0x649<<0);
	LPC_TIM0->TCR = (1<<0);

	NVIC_EnableIRQ(TIMER0_IRQn);
	return;
}


void TIMER0_IRQHandler(void){
	if(boolModo==0){
		LPC_GPIO0->FIOCLR |= (0xF<<6);
		if(LPC_TIM0 -> IR &(1<<0)){
				LPC_TIM0-> IR |= (1<<0);
				LPC_GPIO0->FIOSET = (1<<9);
			}

			if(LPC_TIM0 -> IR &(1<<1)){
				LPC_TIM0-> IR |= (1<<1);
				LPC_GPIO0->FIOSET = (1<<8);
			}

			if(LPC_TIM0 -> IR &(1<<2)){
				LPC_TIM0-> IR |= (1<<2);
				LPC_GPIO0->FIOSET = (1<<7);
			}

			if(LPC_TIM0 -> IR &(1<<3)){
				LPC_TIM0-> IR |= (1<<3);
				LPC_GPIO0->FIOSET = (1<<6);
			}
	}

	if(boolModo==1){
			if(LPC_TIM0 -> IR &(1<<0)){
				LPC_TIM0-> IR |= (1<<0);
				LPC_GPIO0->FIOSET = (1<<9);
				LPC_GPIO0->FIOCLR |= (1<<7);

			}

			if(LPC_TIM0 -> IR &(1<<1)){
				LPC_TIM0-> IR |= (1<<1);
				LPC_GPIO0->FIOSET = (1<<8);
				LPC_GPIO0->FIOCLR |= (1<<6);

			}

			if(LPC_TIM0 -> IR &(1<<2)){
				LPC_TIM0-> IR |= (1<<2);
				LPC_GPIO0->FIOSET = (1<<7);
				LPC_GPIO0->FIOCLR |= (1<<9);

			}

			if(LPC_TIM0 -> IR &(1<<3)){
				LPC_TIM0-> IR |= (1<<3);
				LPC_GPIO0->FIOSET = (1<<6);
				LPC_GPIO0->FIOCLR |= (1<<8);

			}
	}

}

void EINT3_IRQHandler (void){
	retardo(1000000);
	contador++;
	if(contador%2){
		boolModo=1;
	}else{
		boolModo=0;
	}
	LPC_SC->EXTINT|=(1<<2);	//LIMPIO BANDERA
	LPC_TIM0->TC=0;//LA CUENTA EMPIEZA DESDE 0.
}

void retardo(uint32_t tiempo){
	uint32_t conta;
	for(conta=0;conta < tiempo;conta++){}
	return;
}
