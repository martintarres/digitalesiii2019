/*
===============================================================================
 Name        : ejercicio1.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
===============================================================================
*/

#ifdef __USE_CMSIS
#include "LPC17xx.h"
#endif

#include <cr_section_macros.h>

void retardo(int);
int frecuenciaMax=200000;
int frecuenciaMin=1000000;
int frecuenciaActual;
int flagSubiendo=0;

int main(void) {
	int verClk;
	verClk= SystemCoreClock;
	LPC_GPIO0->FIODIR |= (1<<0);
	frecuenciaActual=frecuenciaMax;
	while(1){
		LPC_GPIO0-> FIOSET = (1<<0);
		retardo(frecuenciaActual);
		LPC_GPIO0-> FIOCLR = (1<<0);
		retardo(frecuenciaActual);
	}
	return 0;
}

void retardo(int retardo){
	int conta,ver, *ptr;
	ptr=&conta;
	ver = sizeof(conta);
	if(flagSubiendo==0){
		frecuenciaActual=frecuenciaActual+10000;
		if(frecuenciaActual>frecuenciaMin){
			flagSubiendo=1;
		}
	}

	if(flagSubiendo==1){
		frecuenciaActual=frecuenciaActual-10000;
		if(frecuenciaActual<frecuenciaMax){
			flagSubiendo=0;
		}
	}


	for(conta=0;conta<retardo;conta++){}
}
