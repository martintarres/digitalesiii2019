/*
===============================================================================
 /*
===============================================================================
 Name        : Eje3TarresMartin.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
===============================================================================
*/

#include "LPC17xx.h"

void ConfiguracionPines(void);
void retardo(uint32_t tiempo);
void confIntExt(void);
uint8_t sentido=0;
uint32_t tiempo = 1000000;


int main(void) {

	uint8_t i = 1;
	uint32_t numeroInterrupcion;

	numeroInterrupcion=NVIC_GetPriority(EINT0_IRQn);
	NVIC_SetPriority(EINT0_IRQn,24);
	numeroInterrupcion=NVIC_GetPriority(EINT0_IRQn);

	ConfiguracionPines();
	confIntExt();

    while(1) {
    	if(sentido%2){
    	LPC_GPIO0->FIOPIN0 = (1<<i);
		retardo(tiempo);
		i++;
		if(i>7){
			i=0;
		}
    }
    	else{
    		i--;
    		LPC_GPIO0->FIOPIN0 = (1<<i);
    		retardo(tiempo);
    		if(i==0){
    		i=8;
    	}
    	}
    }
    return 0 ;
}

void retardo(uint32_t tiempo){
	uint32_t conta;
	for(conta=0;conta<tiempo;conta++){}
	return;
}

void ConfiguracionPines(void){
	    LPC_GPIO0->FIODIR0 = 0xFF;
		LPC_GPIO2->FIODIR&=~(1<<10);
		LPC_GPIO2->FIODIR&=~(1<<11);
		LPC_GPIO2->FIODIR&=~(1<<12);

		return;
}

void confIntExt(void){
	LPC_PINCON->PINSEL4 |= (1<<20);
	LPC_PINCON->PINSEL4 |= (1<<22);
	LPC_PINCON->PINSEL4 |= (1<<24);

	LPC_SC-> EXTINT |= (1<<0);
	LPC_SC-> EXTINT |= (1<<1);
	LPC_SC-> EXTINT |= (1<<2);


	LPC_SC-> EXTMODE |= (1<<0);
	LPC_SC-> EXTMODE |= (1<<1);
	LPC_SC-> EXTMODE |= (1<<2);

	LPC_SC-> EXTPOLAR |= (1<<0);
	LPC_SC-> EXTPOLAR |= (1<<1);
	LPC_SC-> EXTPOLAR |= (1<<2);

	NVIC_EnableIRQ(EINT0_IRQn);
	NVIC_EnableIRQ(EINT1_IRQn);
	NVIC_EnableIRQ(EINT2_IRQn);

	return;
}

void EINT0_IRQHandler(void){
	retardo(100000);
	sentido++;
	LPC_SC-> EXTINT |= (1<<0);
}

void EINT1_IRQHandler(void){
	retardo(100000);
	tiempo +=300000;
	LPC_SC-> EXTINT |= (1<<1);
}

void EINT2_IRQHandler(void){
	retardo(100000);
	if(tiempo>400000){
	tiempo -=300000;
	}else{
		if(tiempo>50000){
		tiempo -=100000;
		}
	}
	LPC_SC-> EXTINT |= (1<<2);
}


