/*
===============================================================================
 Name        : eje8_4TarresMartin.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
===============================================================================
*/

#include "LPC17xx.h"

void confGPIO(void); // Prototipo de la funcion de conf. de puertos
void confTimer(void); //Prototipo de la función de conf. de interrupciones externas
void retardo(uint32_t);
uint32_t valorMR0=7000000;

int main(void) {
	confGPIO();
	confTimer();

	while(1){}
	return 0;
}

void confGPIO(void){
	LPC_GPIO0->FIODIR |= (1<<22);
	LPC_GPIO2 -> FIODIR &= ~(1<<12);	//PONGO COMO ENTRADA EL PIN DOCE DEL PUERTO DOS
	LPC_PINCON -> PINSEL4 &= ~(1<<25);	//SELECCIONO EINT2
	LPC_PINCON -> PINSEL4 |= (1<<24);	//SELECCIONO EINT2
	LPC_SC -> EXTMODE |= (1<<2);			//EINT1 POR FLANCOS
	LPC_SC -> EXTPOLAR &= ~(1<<2);			//EINT1 POR FLANCOS DESCENDENTE
	NVIC_EnableIRQ(EINT2_IRQn);

	return;
}

void confTimer(void){
	LPC_SC->PCONP |= (1 << 1); //Por defecto timer 0 y 1 estan siempre prendidos
	LPC_SC->PCLKSEL0 |= (1<<2); // configuración del clock de periférico
	LPC_PINCON->PINSEL3 |= (3<<24);
	LPC_TIM0->EMR|=(3<<4);
	LPC_TIM0->MR0 = valorMR0;
	LPC_TIM0->MCR = 3;
	LPC_TIM0->TCR = 3;
	LPC_TIM0->TCR &= ~(1<<1);
	NVIC_EnableIRQ(TIMER0_IRQn);
	return;
}


void TIMER0_IRQHandler(void) //ISR del timer0
{
	static uint8_t i = 0;
	if (i==0){
	LPC_GPIO0->FIOSET = (1<<22);
	i = 1;
	}
	else if (i==1){
	LPC_GPIO0->FIOCLR = (1<<22);
	i = 0;
	}
	LPC_TIM0->IR|=1; //Limpia bandera de interrupción
}

void EINT2_IRQHandler (void){
	retardo(100000);
	if(valorMR0*2 <= 4294967296){	//ME FIJO SI LLEGUE AL MAXIMO VALOR
	valorMR0=valorMR0*2;
	LPC_TIM0->MR0 = valorMR0;
	}else{
		valorMR0=7000000;
	}

	LPC_SC->EXTINT|=(1<<2);	//LIMPIO BANDERA

}

void retardo(uint32_t tiempo){
	uint32_t conta;
	for(conta=0;conta < tiempo;conta++){}
	return;
}
