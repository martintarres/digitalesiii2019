/*
===============================================================================
 Name        : eje8_2TarresMartin.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
===============================================================================
*/

#include "LPC17xx.h"

void confGPIO(void); // Prototipo de la funcion de conf. de puertos
void confTimer(void); //Prototipo de la función de conf. de interrupciones externas
void retardo(uint32_t);
uint8_t contador=0;

int main(void) {

	confTimer();
	confGPIO();
	while(1){}
	return 0;
}

void confGPIO(void){
	LPC_GPIO0->FIODIR |= (1<<22);
	LPC_GPIO0 -> FIODIR &= ~(1<<0);	//PONGO COMO ENTRADA EL PIN CERO DEL PUERTO CERO
	LPC_GPIOINT -> IO0IntEnF |= (1<<0);	//INTERRUPCIONES DESCENDENTES
	LPC_GPIOINT -> IO0IntClr |= (1<<0);
	NVIC_EnableIRQ(EINT3_IRQn);

	return;
}

void confTimer(void){
	LPC_SC->PCONP |= (1 << 1); //Por defecto timer 0 y 1 estan siempre prendidos
	LPC_SC->PCLKSEL0 |= (1<<2); // configuración del clock de periférico
	LPC_PINCON->PINSEL3 |= (3<<24);
	LPC_TIM0->EMR|=(3<<4);
	LPC_TIM0->MR0 = 70000000;
	LPC_TIM0->MCR = 3;
	LPC_TIM0->TCR = 3;
	LPC_TIM0->TCR &= ~(1<<1);
	NVIC_EnableIRQ(TIMER0_IRQn);
	return;
}


void TIMER0_IRQHandler(void) //ISR del timer0
{
	static uint8_t i = 0;
	if (i==0){
	LPC_GPIO0->FIOSET = (1<<22);
	i = 1;
	}
	else if (i==1){
	LPC_GPIO0->FIOCLR = (1<<22);
	i = 0;
	}
	LPC_TIM0->IR|=1; //Limpia bandera de interrupción
}

void EINT3_IRQHandler (void){
	retardo(1000000);
	if(LPC_GPIOINT -> IO0IntStatF & (1<<0)){
		contador++;

		switch (contador){
			case 0:
				break;
			case 1:
				LPC_SC->PCLKSEL0 |= (1<<3);
				LPC_SC->PCLKSEL0 &= ~(1<<2);
				break;
			case 2:
				LPC_SC->PCLKSEL0 &= ~(1<<3);
				LPC_SC->PCLKSEL0 &= ~(1<<2);
				break;
			case 3:
				LPC_SC->PCLKSEL0 |= (1<<3);
				LPC_SC->PCLKSEL0 |= (1<<2);
				break;
			case 4:
				LPC_SC->PCLKSEL0 &= ~(1<<3);
				LPC_SC->PCLKSEL0 |= (1<<2);
				contador=0;
				break;
		}
		LPC_GPIOINT -> IO0IntClr |= (1<<0);
	}
}

void retardo(uint32_t tiempo){
	uint32_t conta;
	for(conta=0;conta < tiempo;conta++){}
	return;
}

