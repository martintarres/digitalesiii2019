/*
===============================================================================
 Name        : eje10TarresMartin.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
===============================================================================
*/

#include "LPC17xx.h"
void confGPIO(void); // Prototipo de la funcion de conf. de puertos
void confTimer(void); //Prototipo de la función de conf. de interrupciones externas
void configInterr(void);

float aux = 0;
float aux1 = 0;

int main(void) {
	confGPIO();
	confTimer();
	configInterr();
	while(1){

	}
	return 0;
}

void confGPIO(void){
	LPC_GPIO0->FIODIR |= (1<<22);
	LPC_GPIO0->FIOCLR |= (1<<22);
	return;
}

void configInterr(void){
	//SELECCIONO CAP0.0 Y CAP0.1
	LPC_PINCON -> PINSEL3 |= (3<<20);
	LPC_PINCON -> PINSEL3 |= (3<<22);
}

void confTimer(void){
	LPC_SC->PCONP |= (1 << 1); //Por defecto timer 0 y 1 estan siempre prendidos
	LPC_SC->PCLKSEL0 |= (1<<2); // configuración del clock de periférico
	//CONFIGURACION PARA CAP0.0
	LPC_TIM0->CCR |= (3<<0);
	LPC_TIM0->CCR &= ~(1<<0);
	//CONFIGURACION PARA CAP0.1
	LPC_TIM0->CCR |= (3<<4);
	LPC_TIM0->CCR &= ~(1<<3);
	LPC_TIM0->TCR = 3;
	LPC_TIM0->TCR &= ~(1<<1);
	NVIC_EnableIRQ(TIMER0_IRQn);
	return;
}

void TIMER0_IRQHandler(void) //ISR del timer0
{
	float shooter=0;

	if(LPC_TIM0->IR & (1<<4)){
		LPC_GPIO0->FIOSET = (1<<22);
		aux = LPC_TIM0->CR0 * 0.00000001 ; //Variable auxiliar para observar el valor del registro de captura
		LPC_TIM0->IR|=4; //Limpia bandera de interrupción
	}

	if(LPC_TIM0->IR & (1<<5)){
		LPC_GPIO0->FIOCLR = (1<<22);
		aux1 = LPC_TIM0->CR1 * 0.00000001; //Variable auxiliar para observar el valor del registro de captura
		shooter =  (aux1 -aux) ;
		LPC_TIM0->IR|=5; //Limpia bandera de interrupción
	}

	return;
}
