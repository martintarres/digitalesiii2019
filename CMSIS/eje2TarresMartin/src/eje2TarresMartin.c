/*
===============================================================================
 Name        : eje2TarresMartin.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
===============================================================================
*/

#include "LPC17xx.h"


void configPines();
void apagarLeds();
void retardo(uint32_t);

int main(void) {
	uint32_t frecuenciaActual=1000000;
	uint8_t flagDireccion=0;
	uint8_t contadorBoton=0;
	int contadorLed=0;

	configPines();

	while(1){

		if(LPC_GPIO2 -> FIOPIN & (1<<0)){

		}else{
			contadorBoton++;
			rebote(5000000);
		}

		if(LPC_GPIO2 -> FIOPIN & (1<<1)){

		}else{
			frecuenciaActual=frecuenciaActual+1000000;
			rebote(5000000);
		}

		if(LPC_GPIO2 -> FIOPIN & (1<<2)){

		}else{
			frecuenciaActual=frecuenciaActual-1000000;
			rebote(5000000);
		}


		if(contadorBoton%2){
			flagDireccion=1;
		}else{
			flagDireccion=0;
		}

		switch (contadorLed){
			case 0:{
				apagarLeds();
				LPC_GPIO0-> FIOSET = (1<<0);
				break;
			}
			case 1:{
				apagarLeds();
				LPC_GPIO0-> FIOSET = (1<<1);
				break;
			}
			case 2:{
				apagarLeds();
				LPC_GPIO0-> FIOSET = (1<<2);
				break;
			}
			case 3:{
				apagarLeds();
				LPC_GPIO0-> FIOSET = (1<<3);
				break;
			}
			case 4:{
				apagarLeds();
				LPC_GPIO0-> FIOSET = (1<<4);
				break;
			}
			case 5:{
				apagarLeds();
				LPC_GPIO0-> FIOSET = (1<<5);
				break;
			}
			case 6:{
				apagarLeds();
				LPC_GPIO0-> FIOSET = (1<<6);
				break;
			}
			case 7:{
				apagarLeds();
				LPC_GPIO0-> FIOSET = (1<<7);
				break;
			}
		}
		retardo(frecuenciaActual);

		if(flagDireccion==0){
			contadorLed++;
		}else{
			contadorLed--;
		}

		if(contadorLed==8){
			contadorLed=0;
		}

		if(contadorLed==-1){
			contadorLed=7;
		}

	}
}

void configPines(){
	//PONGO PINES COMO SALIDA
	LPC_GPIO0->FIODIR |= (1<<0);
	LPC_GPIO0->FIODIR |= (1<<1);
	LPC_GPIO0->FIODIR |= (1<<2);
	LPC_GPIO0->FIODIR |= (1<<3);
	LPC_GPIO0->FIODIR |= (1<<4);
	LPC_GPIO0->FIODIR |= (1<<5);
	LPC_GPIO0->FIODIR |= (1<<6);
	LPC_GPIO0->FIODIR |= (1<<7);

	//APAGO LOS LED POR LAS DUDAS
	LPC_GPIO0-> FIOCLR = (1<<0);
	LPC_GPIO0-> FIOCLR = (1<<1);
	LPC_GPIO0-> FIOCLR = (1<<2);
	LPC_GPIO0-> FIOCLR = (1<<3);
	LPC_GPIO0-> FIOCLR = (1<<4);
	LPC_GPIO0-> FIOCLR = (1<<5);
	LPC_GPIO0-> FIOCLR = (1<<6);
	LPC_GPIO0-> FIOCLR = (1<<7);

	//CONFIGURO PINES COMO ENTRADA
	LPC_GPIO2 -> FIODIR &= ~(1<<0);
	LPC_GPIO2 -> FIODIR &= ~(1<<1);
	LPC_GPIO2 -> FIODIR &= ~(1<<2);
}

void apagarLeds(){
	LPC_GPIO0-> FIOCLR |= (255<<0);
}

void retardo(uint32_t retardo){
	for(uint32_t conta=0;conta<retardo;conta++){

	}
}

void rebote(uint32_t tiempo){
	for(uint32_t conta=0; conta < tiempo; conta++){

	}
}
